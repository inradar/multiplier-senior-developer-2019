# Multiplier Senior Developer Code Challenge

You should build a simple checkout system. In it the user should be able to add products to a shopping cart and complete the purchase by providing an address. Administrators should be able to create, read, update and delete products, as well as view the list of transactions and financial balance.

## Project Requirements

### Sign Up / Login

As authentication is not the focus of the challenge the user will be identified and logged in only by its a username, without the need for a password.

* **Log in User** - Ask for a username and login the user if it exists.
* **Create User** - Send username and flag indicating if the user is an administrator.
* **Logout User** - Logout user.

### Features available for common user

* **Product List** - View a list containing the name and price of the products and also a button to add the product to the cart.
* **Product Detail** - View the detail containing all the list information and also a product description (not to be provided by the listing endpoint).
* **Shopping Cart** - List of products with respective quantities, total value per product, total purchase value and button to proceed with payment.
* **Payment**
	* Address - CEP field that should autocomplete the other fields through integration with the [ViaCEP API](https://viacep.com.br/).
	* Confirmation - Summary of purchase items, their total value, and the shipping address. By confirming the purchase it is already considered paid by the system and must be added to the balance.

### Features available for administrators

* **Products CRUD**
* **Transaction List** - Must contain transaction ID, user, and total value. Page should contain the total outstanding balance.

## What do we expect?

* Development of a REST API on the backend
* Development of a frontend fully integrated with the API.
* We expect you to code with the language you are more confortable with.
* **We expect you to submit your work even if you not complete the development of all features. We will evaluate all submissions.**

## What should I do?

* Fork this repository (Click on the '+' button on the left menu and then on 'Fork this repository')
* Develop the aplication using best practices (write your code and comments in english)
* Create unit test cases for the whole application
* Document the API using the tool of your choice
* Overwrite this README file and describe how to set up your project and how to run tests
* Create a pull request when you are done

## What is going to be evaluated?

* Code quality
* Test and documentation coverage
* Git usage
* Development time
* Surprising us ;)

## Do you have any questions?

Contact: rafael.reis@inchurch.com.br